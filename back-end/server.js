const express = require('express');
const cors = require('cors');
const app = express();
fileDb.init();

app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/messages');

app.listen(port, () => {
    console.log(`Started server ${port} port!`);
});